﻿namespace ChainOfResponsibility.ExemploDeDescontos
{
    public class CalculadorDeDescontos
    {
        public double Calcula(Orcamento orcamento)
        {
            Desconto PrimeiroDesconto = new DescontoPorQuantidadeItens();
            Desconto SegundoDesconto = new DescontoPorValor();
            Desconto TerceiroDesconto = new DescontoPorVendaCasada();
            Desconto QuartoDesconto = new SemDesconto();

            PrimeiroDesconto.Proximo = SegundoDesconto;
            SegundoDesconto.Proximo = TerceiroDesconto;
            TerceiroDesconto.Proximo = QuartoDesconto;

            return PrimeiroDesconto.Desconta(orcamento);
        }
    }
}
