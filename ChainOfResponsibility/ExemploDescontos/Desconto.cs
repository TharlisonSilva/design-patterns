﻿namespace ChainOfResponsibility.ExemploDeDescontos
{
    public interface Desconto
    {
        double Desconta(Orcamento orcamamento);
        Desconto Proximo { get; set; }
    }
}
