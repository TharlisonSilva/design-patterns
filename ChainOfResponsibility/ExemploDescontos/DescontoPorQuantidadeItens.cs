﻿namespace ChainOfResponsibility.ExemploDeDescontos
{
    public class DescontoPorQuantidadeItens : Desconto
    {
        public Desconto Proximo { get; set; }
        public double Desconta(Orcamento orcamento){

            //Desconto de 10% se a qunatidade itens for maior que 5
            if (orcamento.Itens.Count > 5)
                return orcamento.Valor * 0.1;
            
            // Se não houver desconto então ele passa para o proximo desconto da lista;
            return Proximo.Desconta(orcamento);
        }
    }
}
