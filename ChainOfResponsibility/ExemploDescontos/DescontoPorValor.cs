﻿namespace ChainOfResponsibility.ExemploDeDescontos
{
    public class DescontoPorValor : Desconto
    {
        public Desconto Proximo { get; set; }
        public double Desconta(Orcamento orcamento)
        {
            // Desconto de 7% se o valor da compra for maior do que R$ 500,00
            if (orcamento.Valor> 500)
                return orcamento.Valor * 0.07;

            return Proximo.Desconta(orcamento);
        }
    }
}
