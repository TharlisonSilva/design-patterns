﻿using System.Collections.Generic;

namespace ChainOfResponsibility.ExemploDeDescontos
{
    public class Orcamento
    {
        public double Valor { get; private set; }
        public IList<Item> Itens { get; private set; }

        public Orcamento()
        {
            this.Valor = 0;
            this.Itens = new List<Item>();
        }

        public void AdicionarItem(Item item)
        {
            Itens.Add(item);

        }

        public void DefineValorOrcamento(double valor)
        {
            this.Valor = valor;
        }
    }
}
