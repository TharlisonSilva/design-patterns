﻿namespace ChainOfResponsibility.ExemploDeDescontos
{
    public class SemDesconto : Desconto
    {
        public Desconto Proximo { get; set; }
        public double Desconta(Orcamento orcamamento)
        {
            return 0;
        }
    }
}
