﻿namespace ChainOfResponsibility.ExemploRequisicaoWeb
{
    public class CSV : Formato
    {
        public Formato Proximo { get; set; }
        public string Retorna(Requisicao requisicao, Conta conta)
        {
            if (requisicao.Tipo.Equals(TiposArquivos.CSV))
                return conta.Titular + ";" + conta.saldo;

            return Proximo.Retorna(requisicao, conta);
        }
    }
}
