﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility.ExemploRequisicaoWeb
{
    public class Conta
    {
        public string Titular { get;  set; }
        public double saldo { get; set; }

        public Conta(string nome, double valor)
        {
            this.Titular = nome;
            this.saldo = valor;
        }
        public Conta()
        {
            this.Titular = "";
            this.saldo = 0;
        }
    }
}
