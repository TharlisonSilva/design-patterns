﻿namespace ChainOfResponsibility.ExemploRequisicaoWeb
{
    public class Corrente
    {
        public string RetornaFormato(Requisicao requisicao, Conta conta)
        {
            Formato xml = new XML();
            Formato csv = new CSV();
            Formato porcento = new PORCENTO();
            Formato TipoInvalido = new TipoInvalido();

            xml.Proximo = csv;
            csv.Proximo = porcento;
            porcento.Proximo = TipoInvalido;

            return xml.Retorna(requisicao, conta);
        }
    }
}
