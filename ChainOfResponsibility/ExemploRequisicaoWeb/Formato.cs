﻿namespace ChainOfResponsibility.ExemploRequisicaoWeb
{
    public interface Formato
    {
        Formato Proximo{ get; set; }
        string Retorna(Requisicao requisicao, Conta conta);
    }
}
