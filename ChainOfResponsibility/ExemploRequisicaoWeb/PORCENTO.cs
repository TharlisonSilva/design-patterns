﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility.ExemploRequisicaoWeb
{
    public class PORCENTO : Formato
    {
        public Formato Proximo { get; set; }

        public string Retorna(Requisicao requisicao, Conta conta)
        {
            if (requisicao.Tipo.Equals(TiposArquivos.PORCENTO))
                return "Titular%" + conta.Titular + "%Saldo%" + conta.saldo;
            return Proximo.Retorna(requisicao, conta);
        }
    }
}
