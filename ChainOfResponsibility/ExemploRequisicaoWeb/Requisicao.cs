﻿namespace ChainOfResponsibility.ExemploRequisicaoWeb
{
    public class Requisicao
    {
        public TiposArquivos Tipo { get; set; }
        public Requisicao(TiposArquivos tipo)
        {
            this.Tipo = tipo;
        }
    }
}
