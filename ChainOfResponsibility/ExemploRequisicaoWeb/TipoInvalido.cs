﻿namespace ChainOfResponsibility.ExemploRequisicaoWeb
{
    public class TipoInvalido : Formato
    {
        public Formato Proximo { get; set; }

        public string Retorna(Requisicao requisicao, Conta conta)
        {
            return "Formato invalido";
        }
    }
}
