﻿using System;
using System.Xml.Serialization;

namespace ChainOfResponsibility.ExemploRequisicaoWeb
{
    public class XML : Formato
    {
        public Formato Proximo { get; set; }

        public string Retorna(Requisicao requisicao, Conta conta)
        {
            if (requisicao.Tipo.Equals(TiposArquivos.XML))
            {
                var serial = new XmlSerializer(typeof(Conta));
                serial.Serialize(Console.Out, conta);
                return serial.ToString();
            }
            return Proximo.Retorna(requisicao, conta);
        }
    }
}
