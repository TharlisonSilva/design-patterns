﻿using System;
using ChainOfResponsibility.ExemploDeDescontos;
using ChainOfResponsibility.ExemploRequisicaoWeb;

namespace ChainOfResponsibility
{
    class Program
    {
        static void Main(string[] args)
        {
            //UsoDoChainOfResponsibility_Descontos();
            //UsoDoChainOfResponsibility_RequisicaoWeb();

        }

        public static void UsoDoChainOfResponsibility_Descontos()
        {
            double valorOrcamento = 0;

            CalculadorDeDescontos calculador = new CalculadorDeDescontos();
            Orcamento orcamento = new Orcamento();

            orcamento.AdicionarItem(new Item("CANETA", 100));
            orcamento.AdicionarItem(new Item("ARMARIO", 200));
            orcamento.AdicionarItem(new Item("SSD", 500));
            orcamento.AdicionarItem(new Item("CAMA", 900));
            //orcamento.AdicionarItem(new Item("MEIAS", 10));
            //orcamento.AdicionarItem(new Item("BOTAS", 10));

            for (int i = 0; i < orcamento.Itens.Count; i++)
                valorOrcamento += orcamento.Itens[i].Valor;

            orcamento.DefineValorOrcamento(valorOrcamento);
            double desconto = calculador.Calcula(orcamento);
            Console.WriteLine(desconto);
            Console.ReadKey();
        }

        public static void UsoDoChainOfResponsibility_RequisicaoWeb()
        {
            Conta conta = new Conta("Pessoa01", 200);
            Requisicao TipoRequisicao01 = new Requisicao(TiposArquivos.CSV);
            Requisicao TipoRequisicao02 = new Requisicao(TiposArquivos.XML);
            Requisicao TipoRequisicao03 = new Requisicao(TiposArquivos.PORCENTO);

            Corrente corrente = new Corrente();

            Console.WriteLine(corrente.RetornaFormato(TipoRequisicao01, conta));
            Console.WriteLine("\n\n");
            Console.WriteLine(corrente.RetornaFormato(TipoRequisicao02, conta));
            Console.WriteLine("\n\n");
            Console.WriteLine(corrente.RetornaFormato(TipoRequisicao03, conta));
            Console.ReadKey();
        }
    }
}

/* SUGESTÃO DO USO DESSE PADRÃO
 * 
 * O padrão Chain of Responsibility cai como uma luva quando temos uma lista de comandos a serem executados.
 * De acordo com algum cenário em específico.
 * E sabemos também qual o próximo cenário que deve ser validado, caso o anterior não satisfaça a condição.
 * 
 * Nesses casos, o Chain of Responsibility nos possibilita a separação de responsabilidades,
 * em classes pequenas e enxutas, e ainda provê uma maneira flexível e 
 * desacoplada de juntar esses comportamentos novamente.
 * 
 */
