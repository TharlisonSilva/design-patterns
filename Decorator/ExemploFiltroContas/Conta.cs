﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.ExemploFiltroContas
{
    public class Conta
    {
        public string Titular { get; set; }
        public double Saldo { get; private set; }
        public DateTime DataAbertura { get; set; }

        public Conta(string nome, double valor, DateTime data)
        {
            this.Titular = nome;
            this.Saldo = valor;
            this.DataAbertura = data;
        }

        public double ConsultarSaldo()
        {
            return this.Saldo;
        }

        private void Creditar(double valor)
        {
            if (this.Saldo >= valor)
                this.Saldo = this.Saldo - valor;
            else
                new Exception("Erro ao sacar, saldo insuficiente. \n Seu saldo é: R$ " + this.Saldo);
        }

        private void Debitar(double valor)
        {
            if (valor > 0)
                this.Saldo = valor;
            else
                new Exception("Erro ao Depositar, valor inválido. \n O valor é: R$ " + valor);
        }
    }
}
