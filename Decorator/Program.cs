﻿using System;
using System.Collections.Generic;
using Decorator.ExemploFiltroContas;
using Decorator.ExemploImposto;
using Decorator.ExemploImposto.TiposImpostos;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            //UsoDecorator_ExemploImposto();
            //UsoDecorator_ExemploRelatorio();
        }

        public static void UsoDecorator_ExemploImposto()
        {
            Imposto impostoComplexo = new ISS(new ICMS());

            Orcamento orcamento = new Orcamento(500);

            double valor = impostoComplexo.Calcula(orcamento);

            Console.WriteLine(valor);
            Console.ReadKey();
        }

        public static void UsoDecorator_ExemploRelatorio()
        {
            List<Conta> contas = new List<Conta>();

            for (int i = 0; i < 5; i++)
                contas.Add(new Conta("Antonio nunes de oliveira", 1000, DateTime.Now));

            var itens = new FiltroMesmoMes(new FiltroMenorQue100Reais( new FiltroMaiorQue500MilReais())).Filtra(contas);
            foreach (var item in itens)
            {
                Console.WriteLine("Titutlar: " + item.Titular);
                Console.WriteLine("Saldo: " + item.Saldo);
                Console.WriteLine("Data Abertura: " + item.DataAbertura);
                Console.WriteLine("\n\n");
            }

            Console.ReadKey();

        }
    }
}

/*  SUGESTÃO DO USO DESSE PADRÃO
 * 
 * Sempre que percebemos que temos comportamentos que podem ser compostos por comportamentos de outras classes envolvidas
 * em uma mesma hierarquia, como foi o caso dos impostos, que podem ser composto por outros impostos. 
 * 
 * O Decorator introduz a flexibilidade na composição desses comportamentos, 
 * bastando escolher no momento da instanciação, quais instancias serão utilizadas para realizar o trabalho.
 * 
 */
