﻿using State.Status;

namespace State
{
    public class Orcamento
    {
        public double Valor { get; set; }
        public IEstadoDeUmOrcamento EstadoAtual { get; set; }
        public bool PossuiDesconto { get; set; }

        public Orcamento(double valor)
        {
            this.Valor = valor;
            this.EstadoAtual = new EmAprovacao();
            this.PossuiDesconto = false;
        }

        public void AplicaDescontoExtra() => EstadoAtual.AplicaDescontoExtra(this);
        public void Aprova() => EstadoAtual.Aprova(this);
        public void Reprova() => EstadoAtual.Reprova(this);
        public void Finaliza() => EstadoAtual.Finaliza(this);
    }
}
