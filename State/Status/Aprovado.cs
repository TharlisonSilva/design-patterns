﻿using System;

namespace State.Status
{
    public class Aprovado : IEstadoDeUmOrcamento
    {
        public void AplicaDescontoExtra(Orcamento orcamento)
        {
            if (!orcamento.PossuiDesconto)
            {
                orcamento.Valor -= orcamento.Valor * 0.02;
                orcamento.PossuiDesconto = true;
            }
            else
                throw new Exception("Orcamento só pode receber um desconto nesse estado.");
        }
        public void Aprova(Orcamento orcamento) => throw new Exception("Orçamento já está em estado de aprovação");
        public void Reprova(Orcamento orcamento) => throw new Exception("Orçamento está em estado de aprovação e não pode ser reprovado");
        public void Finaliza(Orcamento orcamento) => orcamento.EstadoAtual = new Finalizado();
    }
}
