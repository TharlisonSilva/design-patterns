﻿using System;

namespace State.Status
{
    public class EmAprovacao : IEstadoDeUmOrcamento
    {
        public void AplicaDescontoExtra(Orcamento orcamento)
        {
            if (!orcamento.PossuiDesconto)
            {
                orcamento.Valor -= orcamento.Valor * 0.05;
                orcamento.PossuiDesconto = true;
            }
            else
                throw new Exception("Orcamento só pode receber um desconto em aprovação");
        }
        public void Aprova(Orcamento orcamento)
        {
            orcamento.EstadoAtual = new Aprovado();
            orcamento.PossuiDesconto = false;
        }
        public void Reprova(Orcamento orcamento) => orcamento.EstadoAtual = new Reprovado();
        public void Finaliza(Orcamento orcamento) => throw new Exception("Orcamento em aprovação não podem ir para finalizado diretamente");
    }
}
