﻿using System;

namespace State.Status
{
    public class Finalizado : IEstadoDeUmOrcamento
    {
        public void AplicaDescontoExtra(Orcamento orcamento) => throw new Exception("Orçamentos finalizados não recebem desconto extra!");
        public void Aprova(Orcamento orcamento) => throw new Exception("Orçamentos finalizados não podem ser aprovados.");
        public void Finaliza(Orcamento orcamento) => throw new Exception("Orçamentos finalizado.");
        public void Reprova(Orcamento orcamento) => throw new Exception("Orçamentos não pode ser finalizado.");
    }
}
