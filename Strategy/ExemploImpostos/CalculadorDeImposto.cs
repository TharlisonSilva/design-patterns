﻿using System;

namespace Strategy.ExemploImpostos
{
    public class CalculadorDeImposto
    {
        public void RealizarCalculo(Orcamento orcamento, Imposto imposto)
        {
            double ResultadoImposto = imposto.Calcular(orcamento);
            Console.WriteLine(ResultadoImposto);
        }
    }
}
