﻿namespace Strategy.ExemploImpostos
{
    public interface Imposto
    {
        double Calcular(Orcamento orcamento);
    }
}
