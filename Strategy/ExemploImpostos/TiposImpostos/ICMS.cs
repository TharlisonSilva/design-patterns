﻿namespace Strategy.ExemploImpostos.TiposImpostos
{
    class ICMS : Imposto
    {
        public double Calcular(Orcamento orcamento)
        {
            return orcamento.Valor * 0.1;
        }
    }
}
