﻿using System;

namespace Strategy.ExemploInvestimentos
{
    class Arrojado : Investimentos
    {
        public double Investir(Conta conta)
        {
            int possibilidade = new Random().Next(1, 10);

            if (possibilidade <= 2)
                return conta.Saldo * 0.05;
            else if (possibilidade <= 5)
                return conta.Saldo * 0.03;
            else
                return conta.Saldo * 0.006;
        }
    }
}
