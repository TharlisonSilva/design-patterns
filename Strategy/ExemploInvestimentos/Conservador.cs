﻿namespace Strategy.ExemploInvestimentos
{
    public class Conservador : Investimentos
    {
        public double Investir(Conta conta) => conta.Saldo * 0.08;
    }
}
