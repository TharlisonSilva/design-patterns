﻿namespace Strategy.ExemploInvestimentos
{
    public interface Investimentos
    {
        double Investir(Conta conta);
    }
}
