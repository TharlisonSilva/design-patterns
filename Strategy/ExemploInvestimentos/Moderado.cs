﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Strategy.ExemploInvestimentos
{
    public class Moderado : Investimentos
    {
        public double Investir(Conta conta)
        {
            int possibilidade = new Random().Next(1,10);
            if(possibilidade <= 5)
                return conta.Saldo * 0.25;
            else
                return conta.Saldo * 0.07;
        }
    }
}
