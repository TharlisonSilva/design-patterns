﻿namespace Strategy.ExemploInvestimentos
{
    public class RealizadorDeInvestimentos
    {
        public double Investir(Conta conta, Investimentos TipoInvestimento) => TipoInvestimento.Investir(conta);
    }
}
