﻿using System;
using Strategy.ExemploInvestimentos;
using Strategy.ExemploImpostos;
using Strategy.ExemploImpostos.TiposImpostos;

namespace Strategy
{
    public class Programa
    {

        static void Main(string[] args)
        {
           //UsoDeEstrategiaDeImpostos();
           //UsoDeEstrategiaDeInvestimentos();
        }

        public static void UsoDeEstrategiaDeImpostos()
        {
            ICMS icms = new ICMS();
            ISS iss = new ISS();

            Orcamento orcamento = new Orcamento(500);

            /*
             * Classe CalculadorDeImposto é usada apenas para exibir o resultado.
             * O calculo dos impostos já está sendo feito dentro de cada imposto.
             * Basta usar o metodo .calcular implementado dento do imposto.
             * 
             */

            CalculadorDeImposto calculardor = new CalculadorDeImposto();
            
            iss.Calcular(orcamento);
            calculardor.RealizarCalculo(orcamento, icms);
            calculardor.RealizarCalculo(orcamento, iss);

            Console.ReadKey();
        }

        public static void UsoDeEstrategiaDeInvestimentos()
        {
            Conta Conta01 = new Conta("Invetidor 01", 500);
            RealizadorDeInvestimentos invest = new RealizadorDeInvestimentos();

            Moderado TipoModerado = new Moderado();
            Conservador TipoConservador = new Conservador();
            Arrojado TipoArrojado = new Arrojado();

            
            // Esse for é para mostrar as varias possibilidades de retorno dos investimentos Moderado e Arrojado.
            // Que pode mudar constatemente.
            // Diferente do conservador que nunca muda, sempre o mesmo percentual.

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Conservador, Investimento : " + invest.Investir(Conta01, TipoConservador));
                Console.WriteLine("Moderado,    Investimento : " + invest.Investir(Conta01, TipoModerado));
                Console.WriteLine("Arrojado,    Investimento : " + invest.Investir(Conta01, TipoArrojado));
                Console.WriteLine("\n");
                Console.ReadKey();
            }
        }
    }
}

/* SUGESTÃO DO USO DESSE PADRÃO
 * 
 * O padrão Strategy é muito útil quando temos um conjunto de algoritmos similares. 
 * E precisamos alternar entre eles em diferentes pedaços da aplicação. 
 * Nos exemplos implementados, temos diferentes maneiras de calcular o imposto, e precisamos alternar entre elas.
 * 
 * O Strategy nos oferece uma maneira flexível para escrever diversos algoritmos diferentes, 
 * e de passar esses algoritmos para classes clientes que precisam deles.
 * 
 * Esses clientes desconhecem qual é o algoritmo "real" que está sendo executado, e apenas manda o algoritmo rodar. 
 * Isso faz com que o código da classe cliente fique bastante desacoplado das implementações concretas de algoritmos, 
 * possibilitando assim com que esse cliente consiga trabalhar com N diferentes algoritmos sem precisar alterar o seu código.
 * 
 */
