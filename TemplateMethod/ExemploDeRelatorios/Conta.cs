﻿using TemplateMethod.ExemploDeRelatorios.Templates;

namespace TemplateMethod.ExemploDeRelatorios
{
    public class Conta
    {
        public string Titular { get; set; }
        public double Saldo { get; private set; }
        public string Agencia { get; private set; }
        public string NumeroConta { get; private set; }

        public Conta(string nome, double valor, string agencia, string numeroConta)
        {
            this.Titular = nome;
            this.Saldo = valor;
            this.Agencia = agencia;
            this.NumeroConta = numeroConta;
        }
    }
}
