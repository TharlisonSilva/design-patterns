﻿using System.Collections.Generic;

namespace TemplateMethod.ExemploDeRelatorios.Templates
{
    public abstract class TemplateMethodRelatorio
    {
        protected abstract void Cabecalho();
        protected abstract void Rodape();
        protected abstract void Corpo(IList<Conta> contas);

        public void Imprime(IList<Conta> contas)
        {
            Cabecalho();
            Corpo(contas);
            Rodape();
        }
    }
}
