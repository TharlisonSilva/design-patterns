﻿namespace TemplateMethod.ExemploImpostos
{
    public interface Imposto
    {
        double Calcular(Orcamento orcamento);
    }
    
}
