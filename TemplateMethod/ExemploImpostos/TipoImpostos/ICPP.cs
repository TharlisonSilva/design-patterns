﻿using TemplateMethod.ExemploImpostos.Templates;

namespace TemplateMethod.ExemploImpostos.TipoImpostos
{
    public class ICPP : TemplateImpostoCondicional
    {
        public override bool DeveUsarMaximaTaxacao(Orcamento orcamneto) => orcamneto.Valor >= 500;
        public override double MaximaTaxacao(Orcamento orcamento) => orcamento.Valor * 0.7;
        public override double MinimaTaxacao(Orcamento orcamento) => orcamento.Valor * 0.05;
    }
}
