﻿using TemplateMethod.ExemploImpostos.Templates;

namespace TemplateMethod.ExemploImpostos.TipoImpostos
{
    public class IKCV : TemplateImpostoCondicional
    {
        public override bool DeveUsarMaximaTaxacao(Orcamento orcamento)
        {
            return orcamento.Valor < 500;
        }

        public override double MaximaTaxacao(Orcamento orcamento) => orcamento.Valor * 0.10;
        public override double MinimaTaxacao(Orcamento orcamento) => orcamento.Valor * 0.06;
    }
}
