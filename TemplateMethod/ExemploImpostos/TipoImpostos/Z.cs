﻿using TemplateMethod.ExemploImpostos.Templates;

namespace TemplateMethod.ExemploImpostos.TipoImpostos
{
    public class Z : TemplateImpostoCondicional
    {
        public override bool DeveUsarMaximaTaxacao(Orcamento orcamneto) => orcamneto.Valor <= 10;
        public override double MaximaTaxacao(Orcamento orcamento) => (orcamento.Valor * 0.13) + 100;
        public override double MinimaTaxacao(Orcamento orcamento) => (orcamento.Valor * 0.01) + 200;
    }
}
