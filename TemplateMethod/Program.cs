﻿using System;
using System.Collections.Generic;
using TemplateMethod.ExemploDeRelatorios;
using TemplateMethod.ExemploImpostos;
using TemplateMethod.ExemploImpostos.TipoImpostos;

namespace TemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            //UsoDeTemplateImpostos();
            //UsoDeTemplateRelatorios();
        }

        public static void UsoDeTemplateImpostos()
        {
            Orcamento orcamento01 = new Orcamento(400);
            Orcamento orcamento02 = new Orcamento(600);
            IKCV impostoICKV = new IKCV();
            ICPP impostoICPP = new ICPP();

            var valorICKV = impostoICKV.Calcular(orcamento01);
            Console.WriteLine("ICKV com orcamento de 400, imposto é: " + valorICKV);

            valorICKV = impostoICKV.Calcular(orcamento02);
            Console.WriteLine("ICKV com orcamento de 600, imposto é: " + valorICKV);


            var valorICPP = impostoICPP.Calcular(orcamento01);
            Console.WriteLine("ICPP com orcamento de 400, imposto é: " + valorICKV);

            valorICPP = impostoICPP.Calcular(orcamento02);
            Console.WriteLine("ICPP com orcamento de 600, imposto é: " + valorICKV);

            Console.ReadKey();
        }

        public static void UsoDeTemplateRelatorios()
        {
            List<Conta> contas = new List<Conta>();
            for(int i = 0; i< 5; i++)
                contas.Add(new Conta("Roberto Nunes de Oliveira", 300, "0000-1", "3365-0"));
            
            RelatorioSimples Simples = new RelatorioSimples();
            Console.WriteLine("Relatorio Simples");
            Simples.Imprime(contas);

            Console.WriteLine("\n\n");

            Console.WriteLine("Relatorio Complexo");
            RelatorioComplexo Complexo = new RelatorioComplexo();
            Complexo.Imprime(contas);

            Console.ReadKey();
        }
    }
}

/* SUGESTÃO DO USO DESSE PADRÃO
 * 
 * Quando temos diferentes algoritmos que possuem estruturas parecidas, o Template Method é uma boa solução. 
 * Com ele, conseguimos definir, em um nível mais macro, a estrutura do algoritmo e deixar "buracos", 
 * que serão implementados de maneira diferente por cada uma das implementações específicas.
 * 
 * Dessa forma, reutilizamos ao invés de repetirmos código, e facilitamos possíveis evoluções, 
 * tanto do algoritmo em sua estrutura macro, quanto dos detalhes do algoritmo, 
 * já que cada classe tem sua responsabilidade bem separada.
 * 
 */


